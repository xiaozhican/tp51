<?php
class Container{
    public static function getInstance($class_name,$params = []){
        //获取反射实例
        $reflector = new ReflectionClass($class_name);
        //获取反射实例构造函数
        $constructor = $reflector->getConstructor();
        //获取反射实例构造函数的形参
        $d_params = [];
        if($constructor){
            foreach ($constructor->getParameters() as $params){
                $class = $params->getClass();
                if($class){
                    //如果参数是一个类，就创建实例并依赖注入
                    $d_params[] = self::getInstance();
                }
            }
        }
        //合并数据
        $d_params = array_merge($d_params,$params);
        return $reflector->newInstanceArgs($d_params);
    }
}
class C{
    public $count = 20;
}
class A{
    public $count = 80;

    public function __construct(C $c)
    {
        $this->count += $c->count;
    }
}
class B{
    protected $count = 1;
    public function __construct(A $a,$count)
    {
        $this->count = $a->count + $count;
    }
    public function getCount(){
        return $this->count;
    }
}
$b = Container::getInstance(B::class,[10]);
var_dump($b->getCount());