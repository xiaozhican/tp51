<?php


namespace app\facade;
use think\Facade;

class Singwa extends Facade
{
    protected static function getFacadeClass()
    {
        return '\app\common\Singwa';
    }
}